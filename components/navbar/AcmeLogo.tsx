import Image from "next/image";
import React from "react";
export const AcmeLogo = () => (
  <Image src="/logo.png" width={40} height={40} alt="" />
);
