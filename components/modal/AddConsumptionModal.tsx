import React, { useState } from "react";
import {
  Modal,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Button,
  useDisclosure,
  Input,
  CheckboxGroup,
} from "@nextui-org/react";
import { CustomCheckbox } from "../checkbox/CustomCheckbox";
import { nanoid } from "nanoid";

type Props = {
  isOpen: boolean;
  onOpenChange: () => void;
  setIsDataAdded: React.Dispatch<React.SetStateAction<boolean>>;
};

const timeStamp = Date.now();
const currentDate = new Date(timeStamp).toLocaleString();

export default function AddConsumptionModal({
  isOpen,
  onOpenChange,
  setIsDataAdded,
}: Props) {
  const [groupSelected, setGroupSelected] = React.useState([]);
  const [cosumeObj, setConsumeObj] = useState({});

  const handleAddConsumption = () => {
    const thisMonthConsumption = {
      id: nanoid(),
      AmenitiesUsed: groupSelected,
      date: currentDate,
      ...cosumeObj,
    };
    let data;
    if (typeof window !== "undefined") {
      data = JSON.parse(localStorage.getItem("consumptionData") as any) || [];
    }
    data.push(thisMonthConsumption);
    setIsDataAdded((prev) => !prev);
    localStorage.setItem("consumptionData", JSON.stringify(data));
  };

  return (
    <Modal
      backdrop="opaque"
      isOpen={isOpen}
      onOpenChange={onOpenChange}
      motionProps={{
        variants: {
          enter: {
            y: 0,
            opacity: 1,
            transition: {
              duration: 0.3,
              ease: "easeOut",
            },
          },
          exit: {
            y: -20,
            opacity: 0,
            transition: {
              duration: 0.2,
              ease: "easeIn",
            },
          },
        },
      }}
    >
      <ModalContent>
        {(onClose) => (
          <>
            <ModalHeader className="flex flex-col gap-1">
              Add Monthly Consumption
            </ModalHeader>
            <ModalBody>
              <Input
                type="text"
                label="RR-NUMBER"
                placeholder="Enter your RR-NUMBER"
                onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                  setConsumeObj({ ...cosumeObj, rr_number: e.target.value })
                }
              />
              <Input
                type="number"
                label="CONSUMPTION(IN LTR)"
                min={0}
                placeholder="Enter water consumption"
                onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                  setConsumeObj({ ...cosumeObj, consumption: e.target.value })
                }
              />
              <div className="flex flex-col gap-1 w-full">
                <CheckboxGroup
                  className="gap-1"
                  label="Select amenities"
                  orientation="horizontal"
                  value={groupSelected}
                  onChange={setGroupSelected as any}
                >
                  <CustomCheckbox value="Washing-machine">
                    Washing-machine
                  </CustomCheckbox>
                  <CustomCheckbox value="Water-purifier">
                    Water purifier
                  </CustomCheckbox>
                  <CustomCheckbox value="kitchen-sink">
                    kitchen sink
                  </CustomCheckbox>
                  <CustomCheckbox value="bathroom">Bathroom</CustomCheckbox>
                  <CustomCheckbox value="pool">Pool</CustomCheckbox>
                </CheckboxGroup>
                <p className="mt-4 ml-1 text-default-500">
                  Selected: {groupSelected.join(", ")}
                </p>
              </div>
            </ModalBody>
            <ModalFooter>
              <Button color="danger" variant="light" onPress={onClose}>
                Close
              </Button>
              <Button
                color="primary"
                onPress={() => {
                  onClose();
                  handleAddConsumption();
                }}
              >
                Add
              </Button>
            </ModalFooter>
          </>
        )}
      </ModalContent>
    </Modal>
  );
}
