import React, { useEffect, useState } from "react";
import {
  Table,
  TableHeader,
  TableColumn,
  TableBody,
  TableRow,
  TableCell,
  RadioGroup,
  Radio,
} from "@nextui-org/react";
import { Breadcrumbs, BreadcrumbItem } from "@nextui-org/react";

const colors = [
  "default",
  "primary",
  "secondary",
  "success",
  "warning",
  "danger",
];
const currentTimeStamp = Date.now();
const currentDate = new Date(currentTimeStamp);

type Props = {
  isDataAdded: boolean;
};

export default function TableContainer({ isDataAdded }: Props) {
  const [selectedColor, setSelectedColor] = React.useState("default");
  const [data, setData] = useState<any>();

  useEffect(() => {
    if (typeof window !== "undefined") {
      const consumedData =
        JSON.parse(localStorage.getItem("consumptionData") as any) || [];
      setData(consumedData);
    }
  }, [isDataAdded]);

  return (
    <div className="flex flex-col gap-3 mt-6">
      <Table
        color={"danger"}
        selectionMode="multiple"
        defaultSelectedKeys={["2", "3"]}
        aria-label="Example static collection table"
      >
        <TableHeader>
          <TableColumn>RR-NUMBER</TableColumn>
          <TableColumn>Consumption {`(Liter)`}</TableColumn>
          <TableColumn>Date</TableColumn>
          <TableColumn>Amenities used</TableColumn>
        </TableHeader>
        {data && data.length > 0 ? (
          <TableBody>
            {data.map((item: any) => (
              <TableRow key={item.id}>
                <TableCell>{item.rr_number}</TableCell>
                <TableCell>{item.consumption}</TableCell>
                <TableCell>{item.date}</TableCell>
                <TableCell>
                  <AmenitiesUsed data={item.AmenitiesUsed} />
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        ) : (
          <TableBody emptyContent={"No Data to display."}>{[]}</TableBody>
        )}
      </Table>
    </div>
  );
}

type AmenitiesProps = {
  data: string[];
};
function AmenitiesUsed({ data }: AmenitiesProps) {
  return (
    <Breadcrumbs
      size="sm"
      //   onAction={(key) => setCurrentPage(key)}
      classNames={{
        list: "gap-2",
      }}
      itemClasses={{
        item: [
          "px-2 py-0.5 border-small border-default-400 rounded-small",
          //   "data-[current=true]:border-foreground data-[current=true]:bg-foreground data-[current=true]:text-background transition-colors",
          //   "data-[disabled=true]:border-default-400 data-[disabled=true]:bg-default-100",
        ],
        separator: "hidden",
      }}
    >
      {data &&
        data.map((item) => <BreadcrumbItem key={item}>{item}</BreadcrumbItem>)}
    </Breadcrumbs>
  );
}
