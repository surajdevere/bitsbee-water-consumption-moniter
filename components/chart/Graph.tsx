import dynamic from "next/dynamic";
import React from "react";
const Chart = dynamic(() => import("react-apexcharts"), {
  ssr: false,
});

const Graph = () => {
  //   const options =
  const series = [
    {
      name: "High - 2023",
      data: [2800, 99000, 3300, 36000, 72000, 3020, 33000],
    },
    // {
    //   name: "mid - 2013",
    //   data: [24, 25, 28, 30, 28, 28, 29],
    // },
    // {
    //   name: "Low - 2013",
    //   data: [12, 11, 14, 18, 17, 13, 13],
    // },
  ];

  return (
    <div className="relative w-full  shadow-graph">
      <Chart
        type="line"
        height={350}
        options={{
          chart: {
            height: 350,
            type: "line",
            dropShadow: {
              enabled: true,
              color: "#000",
              top: 18,
              left: 7,
              blur: 10,
              opacity: 0.2,
            },
            toolbar: {
              show: false,
            },
            zoom: { enabled: false },
            // toolbar: { show: true },
          },

          colors: ["#9A69FD", "#8143FD", "#341B65"],
          dataLabels: {
            enabled: false,
          },
          stroke: {
            curve: "smooth",
          },
          title: {
            text: "Water Consumption Rate",
            align: "left",
            style: { color: "#282828", fontSize: "18px", fontWeight: 500 },
          },
          grid: {
            show: false,
            borderColor: "#e7e7e7",
            row: {
              colors: ["#f3f3f3", "transparent"], // takes an array which will be repeated on columns
              opacity: 0.5,
            },
          },
          markers: {
            //   size: 1,
          },
          xaxis: {
            categories: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul"],
            title: {
              text: "Months",
            },
          },
          yaxis: {
            title: {
              text: "Consumption in liters",
            },
            min: 0,
            max: 100000,
          },
          legend: {
            show: false,
            position: "top",
            horizontalAlign: "right",
            floating: true,
            offsetY: -25,
            offsetX: -5,
          },
        }}
        series={series}
      />
    </div>
  );
};

export default Graph;
