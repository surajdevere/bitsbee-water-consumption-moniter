import React from "react";
import { ImagesSlider } from "./ui/images-slider";
import { motion } from "framer-motion";

function ImageCarousel() {
  const images = ["/images/1.png", "/images/2.png", "/images/3.png"];

  return (
    <ImagesSlider
      className="h-[40rem] lg:w-[100rem] rounded-xl"
      images={images}
    >
      <motion.div
        initial={{
          opacity: 0,
          y: -80,
        }}
        animate={{
          opacity: 1,
          y: 0,
        }}
        transition={{
          duration: 0.6,
        }}
        className="z-50 flex flex-col justify-center items-center"
      ></motion.div>
    </ImagesSlider>
  );
}

export default ImageCarousel;
