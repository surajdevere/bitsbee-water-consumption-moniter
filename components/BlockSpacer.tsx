import React from "react";
import { motion } from "framer-motion";

const BlockSpacer = () => {
  return <motion.div className="h-20" />;
};

export default BlockSpacer;
