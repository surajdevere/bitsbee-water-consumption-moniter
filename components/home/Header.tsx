import { Input } from "@nextui-org/react";
import React from "react";
import { Inter } from "next/font/google";
import ImageCarousel from "../ImageCarousel";

const inter = Inter({ subsets: ["latin"] });

type Props = {
  className?: string;
};

const Header = ({ className }: Props) => {
  return (
    <div
      className={`flex w-full flex-wrap md:flex-nowrap justify-center gap-4 mx-auto ${className}`}
    >
      <ImageCarousel />
    </div>
  );
};

export default Header;
