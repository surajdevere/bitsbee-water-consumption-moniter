import BlockSpacer from "@/components/BlockSpacer";
import Graph from "@/components/chart/Graph";
import AddConsumptionModal from "@/components/modal/AddConsumptionModal";
import TableContainer from "@/components/table/Table";
import {
  Avatar,
  Badge,
  Button,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownTrigger,
  useDisclosure,
} from "@nextui-org/react";
import { useRouter } from "next/router";
import React, { useState } from "react";

const Dashboard = () => {
  const { isOpen, onOpen, onOpenChange } = useDisclosure();
  const [isDataAdded, setIsDataAdded] = useState(false);
  const router = useRouter();

  return (
    <div className="w-full">
      <div className="">
        <div className="flex justify-between">
          <button
            onClick={onOpen}
            className="shadow-maroon hover:shadow-maroon-light hover:bg-bg-maroon hover:opacity-85 px-8 py-2 bg-bg-maroon rounded-md text-white font-light transition duration-200 ease-linear"
          >
            Add Consumption
          </button>

          <Dropdown>
            <DropdownTrigger className="">
              <Button variant="light">
                <Badge
                  content="5"
                  color="danger"
                  shape="circle"
                  showOutline={false}
                >
                  <Avatar
                    isBordered
                    radius="full"
                    src="https://i.pravatar.cc/150?u=a04258a2462d826712d"
                  />
                </Badge>
              </Button>
            </DropdownTrigger>
            <DropdownMenu aria-label="Static Actions">
              <DropdownItem onPress={() => router.push("/")} key="new">
                Home
              </DropdownItem>
              <DropdownItem key="copy">Usage</DropdownItem>
              <DropdownItem key="edit">Settings</DropdownItem>
              <DropdownItem key="delete" className="text-danger" color="danger">
                Logout
              </DropdownItem>
            </DropdownMenu>
          </Dropdown>
        </div>

        <AddConsumptionModal
          isOpen={isOpen}
          onOpenChange={onOpenChange}
          setIsDataAdded={setIsDataAdded}
        />

        <TableContainer isDataAdded={isDataAdded} />
        <BlockSpacer />
        <Graph />
      </div>
    </div>
  );
};

export default Dashboard;
