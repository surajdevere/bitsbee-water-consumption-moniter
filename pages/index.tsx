import Image from "next/image";
import Header from "@/components/home/Header";
import ShortContainerWrapper from "@/layout/ShortContainerWrapper";
import { Inter } from "next/font/google";
import { AuroraBackground } from "@/components/ui/aurora-background";
import { motion } from "framer-motion";
import { useRouter } from "next/router";
import Navbar from "@/components/navbar/Navbar";
import { Spotlight } from "@/components/ui/Spotlight";
import { StickyScroll } from "@/components/ui/sticky-scroll-reveal";
import BlockSpacer from "@/components/BlockSpacer";

const inter = Inter({ subsets: ["latin"] });
const content = [
  {
    title: "We need urban water level management",
    description:
      "By 2050, it could rise to 814 million, constituting 50% of the projected total population. As urban populations grow, the demand for water also rises significantly.",
    content: (
      <div className="h-full w-full  flex items-center justify-center text-white">
        <Image
          src="/images/7.png"
          width={300}
          height={300}
          className="h-full w-full object-cover"
          alt="linear board demo"
        />
      </div>
    ),
  },
  {
    title: "Climate change and Ground water level reduction are real threats",
    description:
      "Climate change-induced increased rainfall intensity, coupled with urbanization, has led to reduced infiltration of rainwater into the ground. This directly affects the recharge process. Intensive groundwater extraction to meet the needs of the growing population exacerbates the situation.",
    content: (
      <div className="h-full w-full  flex items-center justify-center text-white">
        <Image
          src="/images/4.png"
          width={300}
          height={300}
          className="h-full w-full object-cover"
          alt="linear board demo"
        />
      </div>
    ),
  },
  {
    title: "A reason for worry, Globally",
    description:
      "Scarcity and Competition as populations increase and economies expand is a logical expectation. Geopolitical Rivalries with water resources passing through borders also cannot be avoided. The Nile, shared by 11 countries, exemplifies this complexity. Hydro-politics is a real thing! The Indus River Basin (shared by India and Pakistan) has been a point of contention. Using Water as a Weapon and Water Diplomacy have resulted in several International agreements. Highlighting the need for conscientious water consumption.",
    content: (
      <div className="h-full w-full  flex items-center justify-center text-white">
        <Image
          src="/images/5.png"
          width={300}
          height={300}
          className="h-full w-full object-cover"
          alt="linear board demo"
        />
      </div>
    ),
  },
  {
    title: "Towards Net-Zero Goals",
    description:
      "Enhanced mitigation action in the urban water sector is crucial to achieving net-zero goals. Urban water actions that reduce greenhouse gas emissions will vary based on development status, water resource availability, and urban form. We encourage you to monitor your water usage yourselves. As they say, every drop makes an ocean!",
    content: (
      <div className="h-full w-full  flex items-center justify-center text-white">
        <Image
          src="/images/6.png"
          width={300}
          height={300}
          className="h-full w-full object-cover"
          alt="linear board demo"
        />
      </div>
    ),
  },
];

export default function Home() {
  const router = useRouter();
  return (
    <main className={`${inter.className} overflow-y-auto`}>
      {router.asPath === "/" && <Navbar className="top-2" />}

      <AuroraBackground
        className="w-full overflow-y-auto scrollbar-hide"
        showRadialGradient
      >
        <motion.div
          initial={{ opacity: 0.0, y: 40 }}
          whileInView={{ opacity: 1, y: 0 }}
          transition={{
            delay: 0.3,
            duration: 0.8,
            ease: "easeInOut",
          }}
          className="relative flex flex-col gap-4 items-center justify-center px-4"
        >
          <ShortContainerWrapper>
            {/* <Spotlight fill="" /> */}

            <BlockSpacer />
            <BlockSpacer />
            <motion.h3
              initial={{ opacity: 0 }}
              animate={{ opacity: 1 }}
              transition={{ duration: 1 }}
              className="text-transparent bg-clip-text font-bold text-center text-8xl mt-52 bg-gradient-to-r from-blue-500 to-green-500"
            >
              Happy World Water Day
            </motion.h3>

            <motion.p
              initial={{ opacity: 0, y: -20 }}
              animate={{ opacity: 1, y: 0 }}
              transition={{ duration: 1 }}
              className="text-center text-3xl font-semibold text-blue-700  py-4 px-6 rounded-lg"
            >
              Preserve Water, Preserve Life
            </motion.p>
            <StickyScroll content={content} />

            <BlockSpacer />
            <BlockSpacer />
            {/* <Header className="mt-64" /> */}

            <BlockSpacer />
            <BlockSpacer />
          </ShortContainerWrapper>
        </motion.div>
      </AuroraBackground>
    </main>
  );
}
