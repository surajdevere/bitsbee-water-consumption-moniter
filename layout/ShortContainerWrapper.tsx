import React from "react";

type Props = {
  children: React.ReactNode;
};

const ShortContainerWrapper = ({ children }: Props) => {
  return (
    <div className="max-w-screen-xl mx-auto overflow-y-auto">{children}</div>
  );
};

export default ShortContainerWrapper;
