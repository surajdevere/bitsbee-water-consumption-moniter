import Navbar from "@/components/navbar/Navbar";
import Sidebar from "@/components/sidebar/Sidebar";
import { useRouter } from "next/router";
import React from "react";

type Props = {
  children: React.ReactNode;
};

const LayoutWrapper = ({ children }: Props) => {
  const router = useRouter();
  return (
    <div className="bg-bg-primary">
      <div className="h-screen w-screen  flex gap-x-[0.5rem]">
        <div className="hidden lg:block w-[15%]">
          <Sidebar className="w-64" />
        </div>

        <div className="lg:rounded-[1.5rem] h-[96%] p-10 w-[83%] bg-bg-white-secondary shadow-xl my-auto  overflow-y-auto">
          {children}
        </div>
      </div>
    </div>
  );
};

export default LayoutWrapper;
